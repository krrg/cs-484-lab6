#pragma once

#include <vector>
#include "Constants.hpp"

/*
 Const Coordinates creates the list of coordinates for each dimension with
 the correct zoom and centering as specified by the STATE.
*/


class ConstCoordinates {
public:

  std::vector<double> xs;
  std::vector<double> ys;

  ConstCoordinates () {

    xs.resize(STATE.w);
    ys.resize(STATE.h);

    #pragma omp parallel for
    for (int px = 0; px < STATE.w; px++) {
      xs[px] = (px - STATE.w / 2) / STATE.zoom + STATE.centerX;
    }

    #pragma omp parallel for
    for (int py = 0; py < STATE.h; py++) {
      ys[py] = (py - STATE.h / 2) / STATE.zoom + STATE.centerY;
    }
  }

};
