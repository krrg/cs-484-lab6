#pragma once

#include "State.hpp"


const State STATE;

enum Channel {
  GET_NEXT_ROW,
  SEND_ROW_BEGIN,
  SEND_ROW_RED,
  SEND_ROW_GREEN,
  SEND_ROW_BLUE,
  TERMINATE
};
