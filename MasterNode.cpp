#include "MasterNode.hpp"
#include <vector>
#include "Constants.hpp"
#include <boost/mpi.hpp>
#include <iostream>
#include <memory>

namespace mpi = boost::mpi;


void MasterNode::runUntilDone() {

    std::vector<mpi::request> futures;
    uint_fast16_t row;

    mpi::request future_getNextRow = comm.irecv(mpi::any_source, GET_NEXT_ROW);
    mpi::request future_sendRowBegin = comm.irecv(mpi::any_source, SEND_ROW_BEGIN, row);


    while (computedRows.size() < STATE.h) {

        /* Think of this as the server `routes` if you will */

        if (boost::optional<mpi::status> status = future_sendRowBegin.test()) {

            computedRows[row].red.resize(STATE.w);
            futures.push_back(comm.irecv(status->source(), SEND_ROW_RED, computedRows[row].red.data(), STATE.w));

            computedRows[row].green.resize(STATE.w);
            futures.push_back(comm.irecv(status->source(), SEND_ROW_GREEN, computedRows[row].green.data(), STATE.w));

            computedRows[row].blue.resize(STATE.w);
            futures.push_back(comm.irecv(status->source(), SEND_ROW_BLUE, computedRows[row].blue.data(), STATE.w));

            future_sendRowBegin = comm.irecv(mpi::any_source, SEND_ROW_BEGIN, row);
        }

        if (boost::optional<mpi::status> status = future_getNextRow.test()) {

            std::vector<uint_fast16_t> nextBlock;
            getNextRows(nextBlock, 10);

            // 1. Send the size so the client knows how much to allocate for.
            comm.isend(status->source(), GET_NEXT_ROW, nextBlock.size());

            // 2. Send the actual vector of rows we want them to compute.
            comm.isend(status->source(), GET_NEXT_ROW, nextBlock.data(), nextBlock.size());

            future_getNextRow = comm.irecv(mpi::any_source, GET_NEXT_ROW);
        }

    }

    // All work has been assigned.
    mpi::wait_all(futures.begin(), futures.end());

    // At this point, all work has been completed

    future_getNextRow.cancel();
    future_sendRowBegin.cancel();

}



void MasterNode::writeToFile() {
    std::cout << "I am done, and now I am writing to a file." << std::endl;
}

void MasterNode::getNextRows(std::vector<uint_fast16_t>& result, const uint_fast16_t BLOCK_SIZE) {
    for (uint_fast16_t i = 0; i < BLOCK_SIZE && nextRow < STATE.h; i++) {
        result.push_back(nextRow++);
    }
}
