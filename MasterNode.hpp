#pragma once


#include "WorkerNode.hpp"
#include <cstdint>
#include <boost/mpi/communicator.hpp>
#include <memory>
#include <vector>


namespace mpi = boost::mpi;

class MasterNode {
private:

    mpi::communicator comm;
    uint_fast16_t nextRow;
    std::map<uint_fast16_t, Row> computedRows;

public:

    MasterNode () {
        this->nextRow = 0;
    }

    void runUntilDone();
    void writeToFile();
    void getNextRows(std::vector<uint_fast16_t>& result, const uint_fast16_t BLOCK_SIZE);

};
