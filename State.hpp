#pragma once

class State {
    public:
        double centerX;
        double centerY;
        double zoom;
        int maxIterations;
        int w;
        int h;
        State() {
            //centerX = -.75;
            //centerY = 0;
            centerX = -1.186340599860225;
            centerY = -0.303652988644423;
            zoom = 10000;
            maxIterations = 100;
            w = 28000;
            h = 28000;
        }
};
