#include "WorkerNode.hpp"
#include "Constants.hpp"
#include "ConstCoordinates.hpp"
#include <boost/mpi.hpp>
#include <cstdint>
#include <iostream>


namespace mpi = boost::mpi;

WorkerNode::WorkerNode() {



}

inline float iterationsToEscape(double x, double y, int maxIterations) {
    double tempa;
    double a = 0;
    double b = 0;
    for (int i = 0 ; i < maxIterations ; i++) {
        tempa = a*a - b*b + x;
        b = 2*a*b + y;
        a = tempa;
        if (a*a+b*b > 64) {
            // return i; // discrete
            return i - log(sqrt(a*a+b*b))/log(8); //continuous
        }
    }
    return -1;
}

inline int hue2rgb(float t){
    while (t > 360) {
        t -= 360;
    }
    if (t < 60) return 255.*t/60.;
    if (t < 180) return 255;
    if (t < 240) return 255. * (4. - t/60.);
    return 0;
}

void WorkerNode::compute() {

    ConstCoordinates scaledCoords;
    const uint_fast16_t HUE_PER_ITERATION = 5;

    std::cout << "I am node " << comm.rank() << " and I am beginning to compute." << std::endl;


    std::vector<uint_fast16_t> rows;

    getNextRows(rows);

    while (! rows.empty()) {

        for (uint_fast16_t i = 0; i < rows.size(); i++) {

            uint_fast16_t row = rows[i];
            // Initialize
            computedRows[row].red.resize(STATE.w);
            computedRows[row].green.resize(STATE.w);
            computedRows[row].blue.resize(STATE.w);

            // #pragma omp parallel for schedule(dynamic)
            for (uint_fast16_t col = 0; col < STATE.w; col++) {

                uint8_t r = 0, g = 0, b = 0;
                float iterations = iterationsToEscape(scaledCoords.xs[col], scaledCoords.ys[row], STATE.maxIterations);

                if (iterations != -1) {
                    float h = HUE_PER_ITERATION * iterations;
                    r = hue2rgb(h + 120);
                    g = hue2rgb(h);
                    b = hue2rgb(h + 240);
                }

                computedRows[row].red.at(col) = r;
                computedRows[row].green.at(col) = g;
                computedRows[row].blue.at(col) = b;
            }

            this->isendSubResult(row);
        }

        rows.clear();
        getNextRows(rows);
    }
}

void WorkerNode::getNextRows(std::vector<uint_fast16_t>& result) const {
    comm.isend(0, GET_NEXT_ROW);

    uint_fast16_t numRows;
    comm.recv(0, GET_NEXT_ROW, numRows);

    result.resize(numRows);
    comm.recv(0, GET_NEXT_ROW, result.data(), numRows);
}

void WorkerNode::isendSubResult(const uint_fast16_t rowIndex) {

    // std::cout << "Node " << comm.rank() << " is sending " << std::endl;
    comm.isend(0, SEND_ROW_BEGIN, rowIndex);
    comm.isend(0, SEND_ROW_RED, computedRows[rowIndex].red.data(), computedRows[rowIndex].red.size());
    comm.isend(0, SEND_ROW_GREEN, computedRows[rowIndex].green.data(), computedRows[rowIndex].green.size());
    comm.isend(0, SEND_ROW_BLUE, computedRows[rowIndex].blue.data(), computedRows[rowIndex].blue.size());

}
