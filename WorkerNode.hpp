#pragma once

#include <iostream>
#include <boost/mpi/communicator.hpp>
#include <cstdint>
#include <map>


namespace mpi = boost::mpi;


struct Row {
    std::vector<uint8_t> red;
    std::vector<uint8_t> green;
    std::vector<uint8_t> blue;
};


class WorkerNode {

private:
    mpi::communicator comm;

    std::map<uint_fast16_t, Row> computedRows;


public:

    WorkerNode ();

    void compute();
    void getNextRows(std::vector<uint_fast16_t>& result) const;
    void isendSubResult(const uint_fast16_t rowIndex);


};
