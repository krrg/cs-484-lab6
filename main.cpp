#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ImageIO.hpp"
#include "State.hpp"
#include "MasterNode.hpp"
#include "WorkerNode.hpp"
#include <boost/mpi.hpp>
#include <sys/time.h>

namespace mpi = boost::mpi;

const int MAX_WIDTH_HEIGHT = 30000;
const bool DRAW_ON_KEY = true;

double when() {
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main () {

    mpi::environment env(MPI_THREAD_FUNNELED);
    mpi::communicator comm;

    if (comm.rank() == 0) {
        MasterNode node;

        // Start timing here.
        std::cout << "Master node is initiated." << std::endl;
        double startTime = when();

        node.runUntilDone();

        // Stop timing here
        std::cout << "Total time: " << when() - startTime << std::endl;

        node.writeToFile();
    } else {
        WorkerNode node;
        node.compute();
    }

    return 0;
}




// unsigned char *createImage(State state) {
//
//     mpi::environment env;
//     mpi::communicator world;
//
//     if (world.rank() == 0) {
//
//     }
//
//     int w = state.w;
//     int h = state.h;
//
//     if (w > MAX_WIDTH_HEIGHT) w = MAX_WIDTH_HEIGHT;
//     if (h > MAX_WIDTH_HEIGHT) h = MAX_WIDTH_HEIGHT;
//
//     unsigned char r, g, b;
//     unsigned char *img = NULL;
//     if (img) free(img);
//     long long size = (long long)w*(long long)h*3;
//     printf("Malloc w %zu, h %zu,  %zu\n",w,h,size);
//     img = (unsigned char *)malloc(size);
//     printf("malloc returned %X\n",img);

    // We restrict the size of the allocation a bit here
    //  from the original code.



    //  At this point, we diverge slightly from the original code in order to
    //  use MPI.

    // #pragma omp parallel for private(r) private(g) private(b) schedule(guided)
    // for (int px=0; px<w; px++) {
    //   for (int py=0; py<h; py++) {
    //     r = g = b = 0;
    //     float iterations = iterationsToEscape(xs[px], ys[py], state.maxIterations);
    //     if (iterations != -1) {
    //       float h = HUE_PER_ITERATION * iterations;
    //       r = hue2rgb(h + 120);
    //       g = hue2rgb(h);
    //       b = hue2rgb(h + 240);
    //     }
    //     long long loc = ((long long)px+(long long)py*(long long)w)*3;
    //     img[loc+2] = (unsigned char)(r);
    //     img[loc+1] = (unsigned char)(g);
    //     img[loc+0] = (unsigned char)(b);
    //   }
    // }
    // return img;
// }

////////////////////////////////////////////////////////////////////////////////

// void draw(State state) {
//   unsigned char *img = createImage(state);
//   writeImage(img, state.w, state.h);
// }
//
// int main() {
//   State state;
//   draw(state);
// }
